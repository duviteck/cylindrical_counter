package com.iqoption.cylindrical_counter;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;
import static com.iqoption.cylindrical_counter.utils.Utils.dpToPx;
import static java.lang.String.valueOf;

public class CylindricalCounterView extends View {

  private static final String KEY_SUPER_STATE = "superState";
  private static final String KEY_CURRENT_VALUE = "currentValue";
  private static final String KEY_IS_TIMER_ENABLED = "isTimerEnabled";
  private static final String KEY_TIMER_IS_INCREASING = "timerIsIncreasing";

  private static final @ColorInt int DEFAULT_TEXT_COLOR = Color.WHITE;
  private static final int DEFAULT_TEXT_SIZE = dpToPx(40f);
  private static final int DEFAULT_DIGITS_COUNT = 10;
  private static final int DEFAULT_ANIMATION_DURATION = 300;
  private static final int DEFAULT_VERTICAL_GAP = dpToPx(2f);
  private static final int DEFAULT_TIMER_TIMEOUT_MS = 1000;
  private static final int DEFAULT_INIT_VALUE = 1234567890;

  //  private static final Interpolator ANIMATION_INTERPOLATOR = new AccelerateDecelerateInterpolator();
  private static final Interpolator ANIMATION_INTERPOLATOR = new DecelerateInterpolator();

  // For now during animation we can see at most 2 symbols at any time.
  private static final int MAX_VISIBLE_SYMBOLS_DURING_ANIM = 2;

  @NonNull
  private final Paint mDigitsPaint = new Paint(ANTI_ALIAS_FLAG);

  @Nullable
  private AnimationHelper mSwitchAnimationHelper;

  @Nullable
  ScheduledThreadPoolExecutor mTimerExecutor;

  @Nullable
  private ScheduledFuture mScheduledTimer;
  private boolean mScheduledTimerIsIncreasing;

  @Nullable
  private CounterListener mCounterListener;

  private int mDigitPaintHeight;
  private int mDigitPaintWidth;

  private int mDigitsCount;
  private int mAnimDuration;
  private int mVerticalGap;
  private int mTimerTimeoutMs;
  private int mCurrentValue;
  private String[] mCurrentSymbols;

  public CylindricalCounterView(Context context) {
    this(context, null);
  }

  public CylindricalCounterView(Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public CylindricalCounterView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context, attrs);
  }

  private void init(@NonNull Context context, @Nullable AttributeSet attrs) {
    // load attrs
    if (attrs != null) {
      TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CylindricalCounterView, 0, 0);
      try {
        mDigitsPaint.setColor(ta.getColor(
            R.styleable.CylindricalCounterView_textColor, DEFAULT_TEXT_COLOR));
        mDigitsPaint.setTextSize(ta.getDimensionPixelSize(
            R.styleable.CylindricalCounterView_textSize, DEFAULT_TEXT_SIZE));
        mDigitsCount = ta.getInteger(
            R.styleable.CylindricalCounterView_digitsCount, DEFAULT_DIGITS_COUNT);
        mAnimDuration = ta.getInteger(
            R.styleable.CylindricalCounterView_animDuration, DEFAULT_ANIMATION_DURATION);
        mVerticalGap = ta.getDimensionPixelSize(
            R.styleable.CylindricalCounterView_verticalGap, DEFAULT_VERTICAL_GAP);
        mTimerTimeoutMs = ta.getInteger(
            R.styleable.CylindricalCounterView_timerTimeoutMs, DEFAULT_TIMER_TIMEOUT_MS);
        int initValue = ta.getInteger(
            R.styleable.CylindricalCounterView_initValue, DEFAULT_INIT_VALUE);
        setCurrentValue(initValue);
      } finally {
        ta.recycle();
      }
    }

    if (mTimerTimeoutMs < mAnimDuration) {
      showErrorMessage("TimerTimeout can't be less than animation duration. Default values used");
      mTimerTimeoutMs = DEFAULT_TIMER_TIMEOUT_MS;
      mAnimDuration = DEFAULT_ANIMATION_DURATION;
    }

    // prepare rest state
    mDigitsPaint.setTypeface(Typeface.MONOSPACE);

    String demoText = "1234567890";   // looks like the best way to determine proper symbol width
    Rect calcViewSizeRect = new Rect();
    mDigitsPaint.getTextBounds(demoText, 0, demoText.length(), calcViewSizeRect);
    mDigitPaintWidth = calcViewSizeRect.width() / demoText.length();
    mDigitPaintHeight = calcViewSizeRect.height();
  }

  private void setCurrentValue(int value) {
    value = getTruncatedValue(value);
    mCurrentValue = value;

    if (mCurrentSymbols == null) {
      mCurrentSymbols = new String[mDigitsCount];
    }
    valueToSymbols(mCurrentValue, mCurrentSymbols);
  }

  private void valueToSymbols(int value, String[] buffer) {
    int len = buffer.length;
    for (int i = len - 1; i >= 0; i--) {
      buffer[i] = valueOf(value % 10);
      value /= 10;
    }
  }

  @Override
  public Parcelable onSaveInstanceState() {
    Bundle bundle = new Bundle();
    bundle.putParcelable(KEY_SUPER_STATE, super.onSaveInstanceState());
    bundle.putInt(KEY_CURRENT_VALUE, mCurrentValue);
    bundle.putBoolean(KEY_IS_TIMER_ENABLED, mScheduledTimer != null);
    bundle.putBoolean(KEY_TIMER_IS_INCREASING, mScheduledTimerIsIncreasing);
    return bundle;
  }

  @Override
  public void onRestoreInstanceState(Parcelable state) {
    if (state instanceof Bundle) {
      Bundle bundle = (Bundle) state;
      setCurrentValue(bundle.getInt(KEY_CURRENT_VALUE));

      boolean isTimerEnabled = bundle.getBoolean(KEY_IS_TIMER_ENABLED);
      boolean timerIsIncreasing = bundle.getBoolean(KEY_TIMER_IS_INCREASING);
      if (isTimerEnabled) {
        startTimerMode(timerIsIncreasing);
      }

      state = bundle.getParcelable(KEY_SUPER_STATE);
    }
    super.onRestoreInstanceState(state);
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);

    if (mSwitchAnimationHelper != null) {
      drawAnimatingState(canvas, mSwitchAnimationHelper);
    } else {
      drawBaseState(canvas);
    }
  }

  private void drawAnimatingState(Canvas canvas, @NonNull AnimationHelper animHelper) {
    float animPercentPassed = animHelper.getAnimationPercentPassed();
    int directionSign = animHelper.isIncreasingDirection() ? 1 : -1;

    int baseline = mDigitPaintHeight + (getHeight() - mDigitPaintHeight) / 2;
    int symbolHeight = mDigitPaintHeight + mVerticalGap;

    // if direction is positive, column of digits is pulled up, if negative - pulled down
    int symbolBaselineShiftMultiplier = directionSign * symbolHeight;

    for (int i = 0; i < mDigitsCount; i++) {
      String[] symbols = animHelper.getSymbols(i);
      int totalScrollHeight = symbolHeight * (symbols.length - 1);
      int alreadyScrolledHeight = (int) (totalScrollHeight * animPercentPassed);

      int firstSymbolToDraw = alreadyScrolledHeight / symbolHeight;
      int lastSymbolToDraw = Math.min(
          symbols.length - 1, firstSymbolToDraw + MAX_VISIBLE_SYMBOLS_DURING_ANIM - 1);

      // draw at most MAX_VISIBLE_SYMBOLS_DURING_ANIM digits for each counter column
      for (int j = firstSymbolToDraw; j <= lastSymbolToDraw; j++) {
        float y = baseline
            + symbolBaselineShiftMultiplier * j
            - directionSign * alreadyScrolledHeight;
        canvas.drawText(symbols[j], i * mDigitPaintWidth, y, mDigitsPaint);
      }
    }
  }

  private void drawBaseState(Canvas canvas) {
    int baseline = mDigitPaintHeight + (getHeight() - mDigitPaintHeight) / 2;
    for (int i = 0; i < mDigitsCount; i++) {
      canvas.drawText(mCurrentSymbols[i], i * mDigitPaintWidth, baseline, mDigitsPaint);
    }
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    int width = measureWidth(widthMeasureSpec);
    int height = measureHeight(heightMeasureSpec);
    setMeasuredDimension(width, height);
  }

  private int measureWidth(int widthMeasureSpec) {
    int desiredWidth = getDesiredWidth();

    int widthMode = MeasureSpec.getMode(widthMeasureSpec);
    int widthSize = MeasureSpec.getSize(widthMeasureSpec);

    if (widthMode == MeasureSpec.EXACTLY) {
      return widthSize;
    } else if (widthMode == MeasureSpec.AT_MOST) {
      return Math.min(desiredWidth, widthSize);
    } else {
      return desiredWidth;
    }
  }

  private int measureHeight(int heightMeasureSpec) {
    int desiredHeight = getDesiredHeight();

    int heightMode = MeasureSpec.getMode(heightMeasureSpec);
    int heightSize = MeasureSpec.getSize(heightMeasureSpec);

    if (heightMode == MeasureSpec.EXACTLY) {
      return heightSize;
    } else if (heightMode == MeasureSpec.AT_MOST) {
      return Math.min(desiredHeight, heightSize);
    } else {
      return desiredHeight;
    }
  }

  private int getDesiredWidth() {
    return mDigitPaintWidth * mCurrentSymbols.length + getPaddingLeft() + getPaddingRight();
  }

  private int getDesiredHeight() {
    return mDigitPaintHeight + mVerticalGap + getPaddingTop() + getPaddingBottom();
  }

  public void setCounterListener(@Nullable CounterListener counterListener) {
    mCounterListener = counterListener;
  }

  public void animateToValue(int newValueCandidate) {
    final int newValue = getTruncatedValue(newValueCandidate);
    if (newValue == mCurrentValue) {
      showErrorMessage("Target value is already set");
      return;
    }

    if (mSwitchAnimationHelper != null) {
      showErrorMessage("Forbidden: animation in progress");
      return;
    }

    mSwitchAnimationHelper = buildAnimationHelper(newValue);
    mSwitchAnimationHelper.startAnimation();
  }

  @NonNull
  private AnimationHelper buildAnimationHelper(int newValue) {
    ValueAnimator animator = ValueAnimator.ofFloat(0, 1);
    animator.setDuration(mAnimDuration);
    animator.setInterpolator(ANIMATION_INTERPOLATOR);
    animator.addUpdateListener(animation -> invalidate());
    animator.addListener(new Animator.AnimatorListener() {
      @Override
      public void onAnimationStart(Animator animation) {
        invalidate();
      }

      @Override
      public void onAnimationEnd(Animator animation) {
        setCurrentValue(newValue);
        invalidate();
        cancelAnimation();
      }

      @Override
      public void onAnimationCancel(Animator animation) {
        setCurrentValue(newValue);
        invalidate();
        cancelAnimation();
      }

      @Override
      public void onAnimationRepeat(Animator animation) {
      }
    });

    return new AnimationHelper(mCurrentValue, newValue, mDigitsCount, animator);
  }

  public void startTimerMode(boolean isIncreasing) {
    if (mCounterListener != null) {
      mCounterListener.onTimerStarted();
    }

    if (mTimerExecutor == null) {
      mTimerExecutor = new ScheduledThreadPoolExecutor(1);
    }

    int sign = isIncreasing ? 1 : -1;
    mScheduledTimer = mTimerExecutor.scheduleAtFixedRate(
        () -> {
          int newValue = getTruncatedValue(mCurrentValue + sign);
          if (isIncreasing && (mCurrentValue == Integer.MAX_VALUE || newValue < mCurrentValue)) {
            showErrorMessage("Max value is achieved. Timer stopped");
            post(this::stopTimerMode);
          } else if (!isIncreasing && mCurrentValue == 0) {
            showErrorMessage("Min value is achieved. Timer stopped");
            post(this::stopTimerMode);
          } else {
            post(() -> animateToValue(mCurrentValue + sign));
          }
        },
        0, mTimerTimeoutMs, TimeUnit.MILLISECONDS);
    mScheduledTimerIsIncreasing = isIncreasing;
  }

  public void stopTimerMode() {
    if (mScheduledTimer != null) {
      mScheduledTimer.cancel(false);
      mScheduledTimer = null;
    }

    if (mCounterListener != null) {
      mCounterListener.onTimerStopped();
    }
  }

  private void cancelAnimation() {
    if (mSwitchAnimationHelper != null) {
      mSwitchAnimationHelper.cancelAnimation();
      mSwitchAnimationHelper = null;
    }
  }

  /**
   * Truncate value to no more than {@link #mDigitsCount} digits
   */
  private int getTruncatedValue(int value) {
    if (mDigitsCount == 10) {
      return value; // no need to truncate
    }

    int module = (int) Math.pow(10, mDigitsCount);
    int truncatedValue = value % module;
    if (truncatedValue < value) {
      showErrorMessage("Too big value -> truncated to " + mDigitsCount + " digits");
    }

    return truncatedValue;
  }

  private void showErrorMessage(@NonNull String message) {
    if (mCounterListener != null) {
      mCounterListener.onErrorOccurred(message);
    } else {
      Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }
  }


  private static class AnimationHelper {

    @NonNull
    private final ValueAnimator mAnimator;

    @NonNull
    private final List<String[]> mSymbolsToDraw;

    private final boolean mIsIncreasingDirection;

    AnimationHelper(int startValue, int endValue, int digitsCount, @NonNull ValueAnimator animator) {
      mAnimator = animator;
      mIsIncreasingDirection = endValue > startValue;
      mSymbolsToDraw = prepareSymbolsToDraw(startValue, endValue, digitsCount, mIsIncreasingDirection);
    }

    @NonNull
    String[] getSymbols(int i) {
      return mSymbolsToDraw.get(i);
    }

    void startAnimation() {
      mAnimator.start();
    }

    void cancelAnimation() {
      mAnimator.cancel();
    }

    float getAnimationPercentPassed() {
      return mAnimator.getAnimatedFraction();
    }

    boolean isIncreasingDirection() {
      return mIsIncreasingDirection;
    }

    @NonNull
    private List<String[]> prepareSymbolsToDraw(int startValue, int endValue, int digitsCount,
                                                boolean isIncreasingDirection) {
      List<String[]> result = new ArrayList<>(digitsCount);

      for (int i = 0; i < digitsCount; i++) {
        // handle digits from end to beginning
        int startDigit = startValue % 10;
        int endDigit = endValue % 10;

        String[] scrollingSymbols = getScrollingSymbols(startDigit, endDigit, isIncreasingDirection);
        result.add(scrollingSymbols);

        startValue /= 10;
        endValue /= 10;
      }

      Collections.reverse(result);  // reversed result -> normal order result
      return result;
    }

    @NonNull
    private String[] getScrollingSymbols(int startDigit, int endDigit, boolean isIncreasingDirection) {
      if (startDigit == endDigit) {
        return new String[] {valueOf(startDigit)};
      }

      final int startValue;
      final int endValue;
      if (isIncreasingDirection) {
        startValue = startDigit;
        endValue = (endDigit > startDigit) ? endDigit : endDigit + 10;
      } else {
        startValue = (startDigit > endDigit) ? startDigit : startDigit + 10;
        endValue = endDigit;
      }

      int size = Math.abs(startValue - endValue) + 1;
      String[] symbols = new String[size];

      int sign = isIncreasingDirection ? 1 : -1;
      for (int i = 0; i < size; i++) {
        symbols[i] = valueOf((startValue + i * sign) % 10);
      }

      return symbols;
    }
  }


  public interface CounterListener {
    void onTimerStarted();
    void onTimerStopped();
    void onErrorOccurred(@NonNull String message);
  }
}
