package com.iqoption.cylindrical_counter.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.iqoption.cylindrical_counter.BuildConfig;

import java.util.Locale;

import static android.util.Log.DEBUG;
import static android.util.Log.ERROR;
import static android.util.Log.INFO;
import static android.util.Log.VERBOSE;
import static android.util.Log.WARN;
import static android.util.Log.getStackTraceString;

public class Logger {
  private static final String TAG = "IQOption.TestApp";
  private static final int LOG_CHUNK_SIZE = 4000;
  private static final boolean LOG_TO_LOGCAT = BuildConfig.LOG_ENABLED;

  private static void log(int logLevel, @NonNull String format, @Nullable Object... args) {
    if (LOG_TO_LOGCAT) {
      String msg = buildMessage(format, args);
      log(logLevel, msg);
    }
  }

  private static void log(int logLevel, @NonNull Throwable th,
                          @NonNull String format, @Nullable Object... args) {
    if (LOG_TO_LOGCAT) {
      String msg = buildMessage(format, args) + '\n' + getStackTraceString(th);
      log(logLevel, msg);
    }
  }

  private static void log(int logLevel, @NonNull String msg) {
    for (int i = 0, len = msg.length(); i < len; i += LOG_CHUNK_SIZE) {
      int end = Math.min(len, i + LOG_CHUNK_SIZE);
      Log.println(logLevel, TAG, msg.substring(i, end));
    }
  }

  public static void v(String format, Object... args) {
    log(VERBOSE, format, args);
  }

  public static void d(String format, Object... args) {
    log(DEBUG, format, args);
  }

  public static void i(String format, Object... args) {
    log(INFO, format, args);
  }

  public static void w(String format, Object... args) {
    log(WARN, format, args);
  }

  public static void w(Throwable th, String format, Object... args) {
    log(WARN, th, format, args);
  }

  public static void e(String format, Object... args) {
    log(ERROR, format, args);
  }

  public static void e(Throwable th, String format, Object... args) {
    log(ERROR, th, format, args);
  }

  public static void ex(Throwable th) {
    e(th, "");
  }

  public static void wtf(@NonNull String message) {
    w("Unexpected case %s", message);
  }

  /**
   * Formats the caller's provided message and prepends useful info like
   * calling thread ID and method name.
   */
  private static String buildMessage(String format, Object... args) {
    String msg = (args == null || args.length == 0) ? format : String.format(format, args);
    StackTraceElement[] trace = new Throwable().fillInStackTrace().getStackTrace();

    String caller = "<unknown>";
    // Walk up the stack looking for the first caller outside of Log
    // It will be at least two frames up, so start there.
    for (int i = 3; i < trace.length; i++) {
      Class<?> clazz = trace[i].getClass();
      String callingClass = trace[i].getClassName();
      if (!clazz.equals(Logger.class)) {
        callingClass = callingClass.substring(callingClass.lastIndexOf('.') + 1);
        callingClass = callingClass.substring(callingClass.lastIndexOf('$') + 1);
        caller = callingClass + "." + trace[i].getMethodName();
        break;
      }
    }
    return String.format(Locale.US, "[%d] %s: %s", Thread.currentThread().getId(), caller, msg);
  }
}

