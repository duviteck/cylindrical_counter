package com.iqoption.cylindrical_counter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.iqoption.cylindrical_counter.CylindricalCounterView.CounterListener;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements CounterListener {

  @BindView (R.id.cylindrical_counter)
  CylindricalCounterView mCylindricalCounterView;

  @BindView (R.id.new_number_edit_text)
  EditText mNewNumberEditText;

  @BindView (R.id.generate_random_number_btn)
  View mGenerateRandomNumberView;

  @BindView (R.id.start_animation_btn)
  View mStartAnimationView;

  @BindView (R.id.auto_start_animating_checkbox)
  CheckBox mAutoStartAnimatingCheckBox;

  @BindView (R.id.start_inc_timer_btn)
  View mStartIncTimerView;

  @BindView (R.id.start_dec_timer_btn)
  View mStartDecTimerView;

  @BindView (R.id.stop_timer_btn)
  View mStopTimerView;

  View mRootView;

  @NonNull
  private final Random mRandom = new Random();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    ButterKnife.bind(this);
    mRootView = findViewById(android.R.id.content);

    mCylindricalCounterView.setCounterListener(this);

    mGenerateRandomNumberView.setOnClickListener(v -> {
      int randomValue = mRandom.nextInt(Integer.MAX_VALUE);
      mNewNumberEditText.setText(String.valueOf(randomValue));

      if (mAutoStartAnimatingCheckBox.isChecked()) {
        animateToNewValue();
      }
    });

    mStartAnimationView.setOnClickListener(v -> animateToNewValue());

    mStartIncTimerView.setOnClickListener(v -> mCylindricalCounterView.startTimerMode(true));
    mStartDecTimerView.setOnClickListener(v -> mCylindricalCounterView.startTimerMode(false));
    mStopTimerView.setOnClickListener(v -> mCylindricalCounterView.stopTimerMode());
    mStopTimerView.setEnabled(false);
  }

  private void animateToNewValue() {
    int newValue;
    try {
      newValue = Integer.parseInt(mNewNumberEditText.getText().toString());
    } catch (Exception e) {
      onErrorOccurred("Can't parse input number. Perhaps too big");
      return;
    }

    mCylindricalCounterView.animateToValue(newValue);
  }

  @Override
  public void onTimerStarted() {
    changeButtonsAbilityForTimer(true);
  }

  @Override
  public void onTimerStopped() {
    changeButtonsAbilityForTimer(false);
  }

  private void changeButtonsAbilityForTimer(boolean isTimerEnabled) {
    mGenerateRandomNumberView.setEnabled(!isTimerEnabled);
    mStartAnimationView.setEnabled(!isTimerEnabled);
    mStartIncTimerView.setEnabled(!isTimerEnabled);
    mStartDecTimerView.setEnabled(!isTimerEnabled);
    mStopTimerView.setEnabled(isTimerEnabled);
  }

  @Override
  public void onErrorOccurred(@NonNull String message) {
    Snackbar.make(mRootView, message, Snackbar.LENGTH_SHORT).show();
  }

  @Override
  protected void onStop() {
    super.onStop();
    mCylindricalCounterView.stopTimerMode();
  }
}
